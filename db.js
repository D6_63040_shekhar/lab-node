const mysql = require('mysql')

const Pool = mysql.createPool({
  connectionLimit: 20,
  user: 'root',
  password: 'manager',
  database: 'car_db',
  port: 3306,
  host: 'localhost',
  waitForConnections: true,
  queueLimit: 0,
})

module.exports = {
  Pool,
}
