const express=require('express');
const utils=require('../utils');
const db=require('../db');
const router=express.Router();
router.post('/addcar',(req,res)=>{ 
    const{car_id,car_name, company_name, car_price}=req.body;
    console.log(car_id);
    const statement=`insert into car( car_id,car_name, company_name, car_price) values(?,?,?,?);`;
    db.Pool.query(statement,[car_id,car_name, company_name, car_price],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
router.get('/getallcar', (req,res)=>{
    console.log("abc")//to get all cars on home screen
    const statement=`select * from car`;
    db.Pool.query(statement,(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
router.patch('/updatecar/:car_id',(req,res)=>{ 
    console.log("abc")
    const {car_id}=req.params;
    const{car_name, company_name}=req.body;
    const statement=`update car set car_name=?, company_name=? where car_id=?;`;
    db.Pool.query(statement,[car_name, company_name,car_id],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
module.exports=router;
console.log("shekhar")

router.delete('/deletecar/:car_id',(req,res)=>{ 
    console.log("abc")
    const {car_id}=req.params;
    const statement=`delete from car where car_id=?;`;
    db.Pool.query(statement,[car_id],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})